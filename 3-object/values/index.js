export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  const res = Object.values(source);
  return res.reduce((previousValue, currentValue) => {
    return parseInt(previousValue, 10) + parseInt(currentValue, 10);
  });
}
